<?php

use app\models\Estado;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;


/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Registros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estado-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Registro', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
      $gridColumns=[
        'codigopc',
        'serie',
        'usuario',
        'nota',
        'estado_manana',
        'estado_tarde',
      ];

      echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns'=>$gridColumns

      ]); 
    ?>

    <div style="
  margin: auto;
  width: 50%;
  border: 3px solid green;
  padding: 10px;
  text-align:center
  ">Hoy es: <?=  date("d-m-Y");?></div>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'filterModel'=>$searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idprestamo',
            'codigopc',
            'serie',
            'usuario',
            'nota',
            [
                'attribute'=>'estado_manana',
                'label'=>'Estado Mañana',
                'encodeLabel' => true,
                
                'contentOptions' => function ($model, $key, $index, $column) {
                    if($model->estado_manana == "ocupado")

                    return ['style' => 'background-color:#FF0000; color:#FFFFFF'];  
        
                    if($model->estado_manana == "libre")
        
                    return ['style' => 'background-color:#00FF00; color:#FFFFFF'];
                    else
                     
                    return ['style' => 'background-color:#808080; color:#FFFFFF'];
                   
                    
                },
                
            ],
            [
                'attribute'=>'estado_tarde',
                'label'=>'Estado Tarde',
                'encodeLabel' => true,
                
                'contentOptions' => function ($model, $key, $index, $column) {
                    if($model->estado_tarde == "ocupado")

                    return ['style' => 'background-color:#FF0000; color:#FFFFFF'];  
        
                    if($model->estado_tarde == "libre")
        
                    return ['style' => 'background-color:#00FF00; color:#FFFFFF'];
                    else
                     
                    return ['style' => 'background-color:#808080; color:#FFFFFF'];
                },
                
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Estado $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idprestamo' => $model->idprestamo]);
                 }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>


</div>
