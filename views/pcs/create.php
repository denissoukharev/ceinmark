<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Pcs $model */

$this->title = 'Añadir PC';
$this->params['breadcrumbs'][] = ['label' => 'PCs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pcs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
