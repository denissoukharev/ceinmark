<?php

use app\models\Pcs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Registro de Pcs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pcs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir PC', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigopc',
            'serie',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Pcs $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigopc' => $model->codigopc]);
                 }
            ],
        ],
    ]); ?>


</div>
