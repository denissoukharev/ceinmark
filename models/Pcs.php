<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pcs".
 *
 * @property int $codigopc
 * @property int|null $serie
 *
 * @property Estado[] $estados
 */
class Pcs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pcs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigopc'], 'required'],
            [['codigopc', 'serie'], 'integer'],
            [['codigopc'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigopc' => 'Codigopc',
            'serie' => 'Serie',
        ];
    }

    /**
     * Gets query for [[Estados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstados()
    {
        return $this->hasMany(Estado::class, ['codigopc' => 'codigopc']);
    }
}
